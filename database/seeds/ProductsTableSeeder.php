<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Products;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();

         
        $products = [
        	['id' => 1, 'name' => 'Medina Cafe', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/1.jpg', 'basePrice' => '11.99'],
        	['id' => 2, 'name' => 'Japadog', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/2.jpg', 'basePrice' => '12.99'],
        	['id' => 3, 'name' => 'Fable', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/3.jpg', 'basePrice' => '14.99'],
        	['id' => 4, 'name' => 'Marutama Ramen', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/4.jpg', 'basePrice' => '8.99']
        ];

        foreach($products as $product){
    		//Restaurant::create($restaurant);
    		$p = new Products;
    		$p->name = $product['name'];
    		$p->description = $product['description'];
    		$p->photo_cover = $product['photo_cover'];
    		$p->basePrice = $product['basePrice'];
    		$p->save();
		}
		

    }
}

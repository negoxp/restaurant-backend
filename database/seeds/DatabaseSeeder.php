<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\ProductsSizes;
use App\ProductsAdds;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->call('RestaurantsTableSeeder');
        //$this->call('ProductsTableSeeder');

        DB::table('products')->delete();
        DB::table('products_sizes')->delete();
        DB::table('products_adds')->delete();

        $products = [
            ['id' => 5, 'name' => 'Mocha Cashew Butter', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/bev1.jpg', 'basePrice' => 6.99],
            ['id' => 6, 'name' => 'Chocolate Milk', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/bev2.jpg', 'basePrice' => 10.99],
            ['id' => 7, 'name' => 'Lemon Breeze', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/bev3.jpg', 'basePrice' => 4.99],
            ['id' => 8, 'name' => 'Orange Juice', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/bev4.jpg', 'basePrice' => 3.99],
            ['id' => 9, 'name' => 'Cappuccino', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/bev5.jpg', 'basePrice' => 5.99],
            ['id' => 10, 'name' => 'Herbal Tea', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/bev6.jpg', 'basePrice' => 3.99],
            ['id' => 11, 'name' => 'Fruit Smoothie', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/bev7.jpg', 'basePrice' => 7.99],
            ['id' => 12, 'name' => 'Milk Shake', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/bev8.jpg', 'basePrice' => 8.99],
            ['id' => 13, 'name' => 'Stuffed Peppers', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/food1.jpg', 'basePrice' => 15.99],
            ['id' => 14, 'name' => 'Honey Garlic Chicken', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/food2.jpg', 'basePrice' => 12.99],
            ['id' => 15, 'name' => 'Cardamom Maple Salmon', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/food3.jpg', 'basePrice' => 14.99],
            ['id' => 16, 'name' => 'Cheese Burger', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/food4.jpg', 'basePrice' => 10.99],
            ['id' => 17, 'name' => 'Chickpeas', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/food5.jpg', 'basePrice' => 18.99],
            ['id' => 18, 'name' => 'Noodles', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/food6.jpg', 'basePrice' => 16.99],
            ['id' => 19, 'name' => 'Hosh Ruba Pasta', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/food7.jpg', 'basePrice' => 15.99],
            ['id' => 20, 'name' => 'Thai Rice', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/food8.jpg', 'basePrice' => 12.99],
            ['id' => 21, 'name' => 'Shaved Squash Salad', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/food9.jpg', 'basePrice' => 10.99],
            ['id' => 22, 'name' => 'Huevos Rancheros Salad', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/food10.jpg', 'basePrice' => 12.99],
            ['id' => 23, 'name' => 'Angel Cake', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/des1.jpg', 'basePrice' => 8.99],
            ['id' => 24, 'name' => 'Vanilla Ice Cream', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/des2.jpg', 'basePrice' => 10.99],
            ['id' => 25, 'name' => 'Cheese Cake Bar', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/des3.jpg', 'basePrice' => 9.99],
            ['id' => 26, 'name' => 'Pavlova With Blueberry Jam', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/des4.jpg', 'basePrice' => 7.99],
            ['id' => 27, 'name' => 'Cream Cheese Parmesan', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/apt1.jpg', 'basePrice' => 8.99],
            ['id' => 28, 'name' => 'Crescent Rolls', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/apt2.jpg', 'basePrice' => 5.99],
            ['id' => 29, 'name' => 'Kimchi Potato Skins', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/apt3.jpg', 'basePrice' => 12.99],
            ['id' => 30, 'name' => 'Sesame Prane Toast', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/apt4.jpg', 'basePrice' => 11.99],
            ['id' => 31, 'name' => 'Medina Cafe', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/1.jpg', 'basePrice' => 11.99],
            ['id' => 32, 'name' => 'Japadog', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/2.jpg', 'basePrice' => 12.99],
            ['id' => 33, 'name' => 'Fable', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/3.jpg', 'basePrice' => 14.99],
            ['id' => 34, 'name' => 'Marutama Ramen', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/4.jpg', 'basePrice' => 8.99],
        ];

























        foreach($products as $product){
            //Restaurant::create($restaurant);
            $p = new Products;
            $p->name = $product['name'];
            $p->description = $product['description'];
            $p->photo_cover = $product['photo_cover'];
            $p->basePrice = $product['basePrice'];
            $p->save();

            $ps = new ProductsSizes();
            $ps->size = 'Large';
            $ps->price = $product['basePrice'] + 2;
            $p->sizes()->save($ps);

            $ps = new ProductsSizes();
            $ps->size = 'Medium';
            $ps->price = $product['basePrice'] + 1;
            $p->sizes()->save($ps);

            $ps = new ProductsSizes();
            $ps->size = 'Chico';
            $ps->price = $product['basePrice'];
            $p->sizes()->save($ps);

            $pa = new ProductsAdds();
            $pa->name_add = 'Extra Avocado';
            $pa->price = 0.99;
            $p->adds()->save($pa);
            

        }


    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Restaurant;

class RestaurantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	

		DB::table('restaurants')->delete();

        $restaurants = [
        	['id' => 1, 'name' => 'Medina Cafe2', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/1.jpg', 'logo' => '', 'address' => '780 Richards Street Vancouver, BC V6B 3A4','open_hours' => '11:30 am - 10:00 pm','rateAvg' => '4.0','email' => 'ssas@gmail.com','phone' => '(604) 688-8837'],
        	['id' => 2, 'name' => 'Japadog', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/2.jpg', 'logo' => '', 'address' => '899 Burrard Street Vancouver, BC V6Z 2K6','open_hours' => '11:30 am - 10:00 pm','rateAvg' => '4.5','email' => 'ssas@gmail.com','phone' => '(604) 688-8837'],
        	['id' => 3, 'name' => 'Fable', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/3.jpg', 'logo' => '', 'address' => '1944 W 4th Avenue Vancouver, BC V6J 1MS','open_hours' => '11:30 am - 10:00 pm','rateAvg' => '3.0','email' => 'ssas@gmail.com','phone' => '(604) 688-8837'],
        	['id' => 4, 'name' => 'Marutama Ramen', 'description' => '', 'photo_cover' => 'http://floresc.com/restaurantes/4.jpg', 'logo' => '', 'address' => '780 Bidwell Street Vancouver, BC V6G 2J6','open_hours' => '11:30 am - 10:00 pm','rateAvg' => '3.5','email' => 'ssas@gmail.com','phone' => '(604) 688-8837']
        ];

        foreach($restaurants as $restaurant){
    		//Restaurant::create($restaurant);
    		$r = new Restaurant;
    		$r->name = $restaurant['name'];
    		$r->description = $restaurant['description'];
    		$r->photo_cover = $restaurant['photo_cover'];
    		$r->logo = $restaurant['logo'];
    		$r->address = $restaurant['address'];
    		$r->open_hours = $restaurant['open_hours'];
    		$r->rateAvg = $restaurant['rateAvg'];
    		$r->email = $restaurant['email'];
    		$r->phone = $restaurant['phone'];
    		$r->save();
		}

		/*
		for ($x = 0; $x <= 10; $x++) {
	        $restaurant = new Restaurant;
	        $restaurant->name = str_random(10);
	        $restaurant->description = str_random(100);
	        $restaurant->save();
    	}

    	$restaurant = new Restaurant;
	        $restaurant->name = str_random(10);
	        $restaurant->description = str_random(100);
	        $restaurant->save();
		*/


    }
}

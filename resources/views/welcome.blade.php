@extends('layout/app')
 
@section('content')

    <div class="container" ng-controller="RestaurantController"> 

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <button class="btn btn-primary btn-xs pull-right" ng-click="initTask()">Add Product</button>
                    </div>
                        
                    <br/>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        
 
                        <table class="table table-bordered table-striped" ng-if="restaurants.length > 0">
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>photo_cover</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                            <tr ng-repeat="(index, restaurant) in restaurants">
                                <td>
                                    @{{ index + 1 }}
                                </td>
                                <td>@{{ restaurant.name }}</td>
                                <td>
                                    <img src="@{{ restaurant.photo_cover }}" width="50" height="50" />
                                </td>
                                <td>@{{ restaurant.basePrice }}</td>
                                <td>
                                    <button class="btn btn-primary btn-xs" ng-click="initEdit(index)">Edit</button>
                                    <button class="btn btn-danger btn-xs" ng-click="deleteRestaurant(index)" >Delete</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>


          <div class="modal fade" tabindex="-1" role="dialog" id="add_new_restaurant">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Product</h4>
                        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        
                    </div>
                    <div class="modal-body">
 
                        <div class="alert alert-danger" ng-if="errors.length > 0">
                            <ul>
                                <li ng-repeat="error in errors">@{{ error }}</li>
                            </ul>
                        </div>
 
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" ng-model="restaurant.name">
                        </div>
                        <div class="form-group">
                            <label for="photo_cover">Photo</label>
                            <input type="text" name="photo_cover" class="form-control"
                                      ng-model="restaurant.photo_cover"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="basePrice">basePrice</label>
                            <input type="text" name="basePrice" class="form-control"
                                      ng-model="restaurant.basePrice"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" ng-click="addRestaurant()">Save</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <div class="modal fade" tabindex="-1" role="dialog" id="edit_restaurant">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Update Product</h4>

                        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        
                    </div>
                    <div class="modal-body">

                        <div class="alert alert-danger" ng-if="errors.length > 0">
                            <ul>
                                <li ng-repeat="error in errors">@{{ error }}</li>
                            </ul>
                        </div>

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" ng-model="edit_restaurant.name">
                        </div>
                        <div class="form-group">
                            <label for="photo_cover">Photo</label>
                            <input type="text" name="photo_cover" class="form-control"
                                      ng-model="edit_restaurant.photo_cover"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="basePrice">basePrice</label>
                            <input type="text" name="basePrice"  class="form-control"
                                      ng-model="edit_restaurant.basePrice"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" ng-click="updateRestaurant()">Submit</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


    </div>
@endsection

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import 'angular';

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

angular.module('RestaurantCRUD', []).controller('RestaurantController', function ($scope,$http) {

	$scope.restaurants = [];
	 
	// List tasks
	$scope.loadRestaurants = function () {
	    $http.get('/jfc/restaurant')
	        .then(function success(e) {
	            $scope.restaurants = e.data.restaurants;
	        });
	};
	$scope.loadRestaurants(); 



	$scope.errors = [];
 
    $scope.restaurant = {
        name: '',
        photo_cover: '',
        basePrice: ''
    };
    $scope.initTask = function () {
        $scope.resetForm();
        $("#add_new_restaurant").modal('show');
    };
 
    // Add new Task
    $scope.addRestaurant = function () {
        $http.post('/jfc/restaurant', {
            name: $scope.restaurant.name,
            photo_cover: $scope.restaurant.photo_cover,
            basePrice: $scope.restaurant.basePrice
        }).then(function success(e) {
            $scope.resetForm();
            $scope.restaurants.push(e.data.restaurant);
            $("#add_new_restaurant").modal('hide');
        }, function error(error) {
            $scope.recordErrors(error);
        });
    };
  
    $scope.recordErrors = function (error) {
        $scope.errors = [];
        if (error.data.errors.name) {
            $scope.errors.push(error.data.errors.name[0]);
        }
 
        if (error.data.errors.photo_cover) {
            $scope.errors.push(error.data.errors.photo_cover[0]);
        }

        if (error.data.errors.basePrice) {
            $scope.errors.push(error.data.errors.basePrice[0]);
        }
    };
 
    $scope.resetForm = function () {
        $scope.restaurant.name = '';
        $scope.restaurant.photo_cover = '';
        $scope.restaurant.basePrice = '';
        $scope.errors = [];
    };


    // initialize update action
    $scope.initEdit = function (index) {
        $scope.errors = [];
        $scope.edit_restaurant = $scope.restaurants[index];
        $("#edit_restaurant").modal('show');
    };
 
    // update the given task
    $scope.updateRestaurant = function () {
        $http.patch('/jfc/restaurant/' + $scope.edit_restaurant.id, {
            name: $scope.edit_restaurant.name,
            photo_cover: $scope.edit_restaurant.photo_cover,
            basePrice: $scope.edit_restaurant.basePrice
        }).then(function success(e) {
            $scope.errors = [];
            $("#edit_restaurant").modal('hide');
        }, function error(error) {
            $scope.recordErrors(error);
        });
    };


    $scope.deleteRestaurant = function (index) {
 	
        var conf = confirm("Do you really want to delete this task?");
 
        if (conf === true) {
            $http.delete('/jfc/restaurant/' + $scope.restaurants[index].id)
                .then(function success(e) {
                    $scope.restaurants.splice(index, 1);
                });
        }
    };




});

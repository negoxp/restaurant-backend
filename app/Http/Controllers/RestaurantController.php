<?php

namespace App\Http\Controllers;

use App\Products;
use App\ProductsSizes;
use App\ProductsAdds;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    //

    public function index()
    {
        /*
        $restaurant = new Restaurant;
        $restaurant->name = "test";
        $restaurant->save();

        echo "holas";
        exit();
       	*/

       	$restaurants = Products::all();
 
        return response()->json([
            'restaurants' => $restaurants,
        ], 200);

        //return view('restaurant/index');
    }


    /**
     * Create a new flight instance.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name'        => 'required|max:255',
            'photo_cover' => 'required',
            'basePrice' => 'required',
        ]);

        $p = new Products;
        $p->name = $request->name;
        $p->photo_cover = $request->photo_cover;
        $p->basePrice = $request->basePrice;
        $p->save();

        $ps = new ProductsSizes();
        $ps->size = 'Large';
        $ps->price = $request->basePrice + 2;
        $p->sizes()->save($ps);

        $ps = new ProductsSizes();
        $ps->size = 'Medium';
        $ps->price = $request->basePrice + 1;
        $p->sizes()->save($ps);

        $ps = new ProductsSizes();
        $ps->size = 'Chico';
        $ps->price = $request->basePrice;
        $p->sizes()->save($ps);

        $pa = new ProductsAdds();
        $pa->name_add = 'Extra Avocado';
        $pa->price = 0.99;
        $p->adds()->save($pa);
 
        return response()->json([
            'restaurant'    => $p,
            'message' => 'Success'
        ], 200);

    }

    public function update(Request $request, Products $restaurant)
    {
        $this->validate($request, [
            'name'        => 'required|max:255',
            'photo_cover' => 'required',
        ]);

        $restaurant->name = $request->name;
        $restaurant->photo_cover = $request->photo_cover;
        $restaurant->basePrice = $request->basePrice;
        $restaurant->save();

        return response()->json([
            'message' => 'Restaurant updated successfully!'
        ], 200);
    }



    public function destroy(Products $restaurant)
    {
        $restaurant->delete();

        return response()->json([
            'message' => 'Restaurant deleted successfully!'
        ], 200);
    }

}

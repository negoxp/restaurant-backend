<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;

class ProductsAdds extends Model
{
    use SyncsWithFirebase;

    public function product()
    {
    	return $this->belongsTo('App\Products');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;

class Products extends Model
{
    use SyncsWithFirebase;

    public function sizes()
    {
    	return $this->hasMany('App\ProductsSizes');
    }
    public function adds()
    {
    	return $this->hasMany('App\ProductsAdds');
    }
}

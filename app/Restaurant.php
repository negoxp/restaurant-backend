<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;

class Restaurant extends Model
{
    use SyncsWithFirebase;
}
